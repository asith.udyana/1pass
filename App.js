import React, { useState } from 'react';
import * as Font from 'expo-font';
import * as SplashScreen from 'expo-splash-screen';
import LoginStackContainer from './assets/routes/loginStack';

SplashScreen.preventAutoHideAsync();

const getFonts = () => Font.loadAsync({
  'nunito-regular': require('./assets/fonts/Nunito-Regular.ttf'),
  'nunito-bold': require('./assets/fonts/Nunito-Bold.ttf')
});

export default function App() {
  const [fontsLoaded, setFontsLoaded] = useState(false);
  const [isAuthenticated, setAuthenticated] = useState(false);

  async function prepare() {
    try {
      await getFonts();
    } catch (e) {
      console.warn(e);
    } finally {
      setFontsLoaded(true);
    }
  }

  if ( fontsLoaded ) {
    SplashScreen.hideAsync();
    return (
      <LoginStackContainer isAuthenticated={isAuthenticated} setAuthenticated={setAuthenticated} />
    );
  } else {
    prepare();
  }
}