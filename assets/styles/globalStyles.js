import { StyleSheet } from "react-native";

export const globalStyles = StyleSheet.create({
  container: { 
    flex: 1,
    alignItems: 'center', 
    justifyContent: 'center',
    alignSelf: 'stretch'
  },
  marginTop: {
    margin: 10,
  },
  inputFeild: {
    borderWidth: 1,
    borderEndColor: '#ddd',
    padding: 10,
    fontSize: 18,
    borderRadius: 6,
  },
  inputErrorText: {
    color: 'crimson',
    fontWeight: 'bold',
    marginBottom: 10,
    textAlign: 'center',
  },
  linkText: {
    fontFamily: 'nunito-bold',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#5532AE'
  },
  row: {
    flexDirection: 'row'
  },
})