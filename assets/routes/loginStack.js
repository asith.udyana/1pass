import React from "react";
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';
import { HomeScreen } from "../screens/home/home";
import { LoginScreen } from "../screens/login/login";
import { RegisterScreen } from "../screens/login/register";
import { ResetPasswordScreen } from "../screens/login/resetPassword";

const LoginStack = createNativeStackNavigator();

function LoginStackContainer( { isAuthenticated, setAuthenticated } ) {
  return (
    <NavigationContainer>
      <LoginStack.Navigator initialRouteName={(isAuthenticated)?'Home':'Login'} screenOptions={{headerShown: false}}>
        <LoginStack.Screen 
          name="Login" 
          component={LoginScreen} 
          options={{title: 'Login Screen'}}
          //initialParams={{ setAuthenticated: setAuthenticated }}
        />
        <LoginStack.Screen name="Register" component={RegisterScreen} options={{title: 'Register Screen'}} />
        <LoginStack.Screen name="ResetPassword" component={ResetPasswordScreen} options={{title: 'Reset Password Screen'}} />
        <LoginStack.Screen name="Home" component={HomeScreen} />
      </LoginStack.Navigator>
    </NavigationContainer>
  );
}

export default LoginStackContainer;

