import React from "react";
import { View, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard, Alert, ScrollView } from 'react-native';
import LoginStackHeaderText from '../../components/loginStackHeaderText';
import LoginStackOnePassLogo from "../../components/logo";
import FlatButton from "../../components/button";
import { globalStyles } from '../../styles/globalStyles';
import { Formik } from 'formik';
import * as yup from 'yup';
import '../../consts/baseURL';

const getLoginApiAsync = async (pnumber, password, actions) => {
  try {
    const response = await fetch(baseURL+'loginUser', {
      method: 'POST',
      headers: {
        Accept: '*/*',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        pnumber: pnumber,
        password: password,
      }),
    });
    const json = await response.json();
    Alert.alert(
      'Response Array',
      'Status : ' + json.status + '\n\nMessage : ' + json.msg + '\n\nToken : ' + json.token
    );
  } catch (error) {
    console.error(error);
  }
};

const inputSchema = yup.object({
  mobile: yup.string()
    .required('Mobile Number should not be empty')
    .typeError('Invalid format')
    .min(10, 'Invalid format')
    .max(10, 'Invalid format'),
  password: yup.string()
    .required('Password should not be empty')
    .min(8, 'Password must contain 8 or more characters with at least one of each: uppercase, lowercase, number and special')
    .matches(/[0-9]/, 'Password requires a number')
    .matches(/[a-z]/, 'Password requires a lowercase letter')
    .matches(/[A-Z]/, 'Password requires an uppercase letter')
    .matches(/[^\w]/, 'Password requires a symbol'),
});

export function LoginScreen({ navigation, route }) {
  return (
    <TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
      <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
        <View style={globalStyles.container}>
          <LoginStackOnePassLogo />
          <LoginStackHeaderText txt='Login' />
          <View style={[globalStyles.marginTop, {width: 300, maxWidth: '75%'}]}>
            <Formik
              initialValues={{ mobile: '', password: '' }}
              validationSchema={inputSchema}
              onSubmit={(values, actions) => {
                //actions.resetForm();
                getLoginApiAsync(values.mobile, values.password, actions);
                // Alert.alert(
                //   'Login Successfull',
                // );
              }}
            >
              {(props) => (
                <View>
                  <TextInput 
                    style={globalStyles.inputFeild}
                    placeholder='Mobile Number'
                    onChangeText={props.handleChange('mobile')}
                    value={props.values.mobile}
                    onBlur={props.handleBlur('mobile')}
                    keyboardType='phone-pad'
                  />
                  <Text style={globalStyles.inputErrorText}>{ props.touched.mobile && props.errors.mobile }</Text>
                  <TextInput 
                    style={globalStyles.inputFeild}
                    placeholder='Password'
                    onChangeText={props.handleChange('password')}
                    value={props.values.password}
                    onBlur={props.handleBlur('password')}
                    secureTextEntry
                  />
                  <Text style={[globalStyles.inputErrorText, {marginBottom: 3}]}>{ props.touched.password && props.errors.password }</Text>
                  <View style={[globalStyles.row, {justifyContent: 'flex-end'}]}>
                    <TouchableOpacity onPress={ () => navigation.navigate('ResetPassword') }>
                      <Text>Forgot your password?</Text>
                    </TouchableOpacity>
                  </View>
                  <FlatButton text='Login' onPress={props.handleSubmit} />
                </View>
              )}
            </Formik>
            <View style={[globalStyles.marginTop, globalStyles.row]}>
              <Text>Don't have an account? </Text>
              <TouchableOpacity onPress={ () => navigation.navigate('Register') }>
                <Text style={globalStyles.linkText}> Sign up</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </ScrollView>
    </TouchableWithoutFeedback>
  );
}