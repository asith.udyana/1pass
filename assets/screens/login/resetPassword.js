import React from "react";
import { View, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard, Alert, ScrollView } from 'react-native';
import LoginStackHeaderText from '../../components/loginStackHeaderText';
import LoginStackOnePassLogo from "../../components/logo";
import HeaderBackBtn from "../../components/headerBackBtn";
import FlatButton from "../../components/button";
import { globalStyles } from '../../styles/globalStyles';
import { Formik } from 'formik';
import * as yup from 'yup';

const inputSchema = yup.object({
  mobile: yup.string()
    .required('Mobile Number should not be empty')
    .typeError('Invalid format')
    .min(10, 'Invalid format')
    .max(10, 'Invalid format'),
});

export function ResetPasswordScreen({ navigation, route }) {
  return (
    <TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
      <View style={{flex: 1}}>
        <HeaderBackBtn goBack={navigation.goBack} />
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={globalStyles.container}>
            <LoginStackOnePassLogo />
            <LoginStackHeaderText txt='Reset Password' />
            <View style={[globalStyles.marginTop, {width: 300, maxWidth: '75%'}]}>
              <Formik
                initialValues={{ mobile: '' }}
                validationSchema={inputSchema}
                onSubmit={(values, actions) => {
                  actions.resetForm();
                  Alert.alert(
                    'Password reset OTP has been sent',
                  );
                }}
              >
                {(props) => (
                  <View>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='Mobile Number'
                      onChangeText={props.handleChange('mobile')}
                      value={props.values.mobile}
                      onBlur={props.handleBlur('mobile')}
                      keyboardType='phone-pad'
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.mobile && props.errors.mobile }</Text>
                    <FlatButton text='Next' onPress={props.handleSubmit} />
                  </View>
                )}
              </Formik>
              <View style={globalStyles.marginTop}>
                <View style={[globalStyles.row]}>
                  <Text>Already have an account? </Text>
                  <TouchableOpacity onPress={ () => navigation.navigate('Login') }>
                    <Text style={globalStyles.linkText}> Login</Text>
                  </TouchableOpacity>
                </View>
                <View style={[globalStyles.row]}>
                  <Text>Don't have an account? </Text>
                  <TouchableOpacity onPress={ () => navigation.navigate('Register') }>
                    <Text style={globalStyles.linkText}> Sign up</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
}