import React from "react";
import { View, Text, TextInput, TouchableOpacity, TouchableWithoutFeedback, Keyboard, ScrollView, Alert } from 'react-native';
import LoginStackHeaderText from '../../components/loginStackHeaderText';
import LoginStackOnePassLogo from "../../components/logo";
import HeaderBackBtn from "../../components/headerBackBtn";
import FlatButton from "../../components/button";
import { globalStyles } from '../../styles/globalStyles';
import { Formik } from 'formik';
import * as yup from 'yup';
import '../../consts/baseURL';

const inputSchema = yup.object({
  fname: yup.string()
    .required('First name should not be empty'),
  lname: yup.string()
    .required('Last name should not be empty'),
  mobile: yup.string()
    .required('Mobile Number should not be empty')
    .typeError('Invalid format')
    .min(10, 'Invalid format')
    .max(10, 'Invalid format'),
  email: yup.string()
    .required('E-mail address should not be empty')
    .email('Invalid e-mail address format'),
  password: yup.string()
    .required('Password should not be empty')
    .min(8, 'Password must contain 8 or more characters with at least one of each: uppercase, lowercase, number and special')
    .matches(/[0-9]/, 'Password requires a number')
    .matches(/[a-z]/, 'Password requires a lowercase letter')
    .matches(/[A-Z]/, 'Password requires an uppercase letter')
    .matches(/[^\w]/, 'Password requires a symbol'),
  confirmPassword: yup.string()
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});

export function RegisterScreen({ navigation, route }) {
  return (
    <TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
      <View style={{flex: 1}}>
        <HeaderBackBtn goBack={navigation.goBack} />
        <ScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: 'center' }}>
          <View style={[globalStyles.container, {marginBottom: 20}]}>
            <LoginStackOnePassLogo />
            <LoginStackHeaderText txt='Create Account' />
            <View style={[globalStyles.marginTop, {width: 300, maxWidth: '75%'}]}>
              <Formik
                initialValues={{ fname: '', lname: '', mobile: '', email: '', password: '', confirmPassword: '' }}
                validationSchema={inputSchema}
                onSubmit={(values, actions) => {
                  actions.resetForm();
                  Alert.alert(
                    'Registration Successfull',
                  );
                }}
              >
                {(props) => (
                  <View>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='First Name'
                      onChangeText={props.handleChange('fname')}
                      value={props.values.fname}
                      onBlur={props.handleBlur('fname')}
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.fname && props.errors.fname }</Text>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='Last Name'
                      onChangeText={props.handleChange('lname')}
                      value={props.values.lname}
                      onBlur={props.handleBlur('lname')}
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.lname && props.errors.lname }</Text>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='Mobile Number'
                      onChangeText={props.handleChange('mobile')}
                      value={props.values.mobile}
                      onBlur={props.handleBlur('mobile')}
                      keyboardType='phone-pad'
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.mobile && props.errors.mobile }</Text>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='E-mail address'
                      onChangeText={props.handleChange('email')}
                      value={props.values.email}
                      onBlur={props.handleBlur('email')}
                      keyboardType='email-address'
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.email && props.errors.email }</Text>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='Password'
                      onChangeText={props.handleChange('password')}
                      value={props.values.password}
                      onBlur={props.handleBlur('password')}
                      secureTextEntry
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.password && props.errors.password }</Text>
                    <TextInput 
                      style={globalStyles.inputFeild}
                      placeholder='Confirm Password'
                      onChangeText={props.handleChange('confirmPassword')}
                      value={props.values.confirmPassword}
                      onBlur={props.handleBlur('confirmPassword')}
                      secureTextEntry
                    />
                    <Text style={globalStyles.inputErrorText}>{ props.touched.confirmPassword && props.errors.confirmPassword }</Text>
                    <FlatButton text='Register' onPress={props.handleSubmit} />
                  </View>
                )}
              </Formik>
              <View style={[globalStyles.marginTop, globalStyles.row]}>
                <Text>Already have an account? </Text>
                <TouchableOpacity onPress={ () => navigation.navigate('Login') }>
                  <Text style={globalStyles.linkText}> Login</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    </TouchableWithoutFeedback>
  );
}