import React from "react";
import { View, Text, Button } from 'react-native';
import { globalStyles } from '../../styles/globalStyles';

export function HomeScreen({navigation, route}) {
  return (
    <View style={globalStyles.container}>
      <Text>Home Screen</Text>
      <Button
        title="Go to Login"
        onPress={ () => navigation.navigate('Login')}
      />
    </View>
  );
}