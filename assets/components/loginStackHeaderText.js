import React from "react";
import { Text } from 'react-native';

export default function LoginStackHeaderText( {txt} ) {
  return (
    <Text 
      style={{
        fontFamily: 'nunito-bold',
        fontSize: 25,
        fontWeight: 'bold',
        letterSpacing: 1.5,
        color: '#5532AE'
      }}
    >
      {txt}
    </Text>
  );
}