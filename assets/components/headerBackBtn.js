import React from "react";
import { View, TouchableOpacity } from 'react-native'
import { MaterialIcons } from '@expo/vector-icons';

export default function HeaderBackBtn({goBack}) {
  return (
    <View style={{
      top: 0,
      alignSelf: 'stretch', 
      right: 0, 
      left: 0,
      height: 40,
    }}>
      <TouchableOpacity
        style={{
          position: 'absolute',
          top: 8,
          left: 8,
        }}
        onPress={goBack}
      >
        <MaterialIcons 
          name='west' 
          size={24}
          style={{ width: 24, height: 24 }}
        />
      </TouchableOpacity>
    </View>
  );
}