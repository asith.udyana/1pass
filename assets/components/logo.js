import React from "react";
import { Image } from 'react-native';

export default function LoginStackOnePassLogo( ) {
  return (
    <Image 
      source={require('../imgs/1Pass.png')} 
      style={{
        width: 250,
        height: 100,
        resizeMode: 'contain'
      }}
    />
  );
}